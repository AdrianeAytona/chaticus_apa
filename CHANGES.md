** CHANGES.MD **

** Chaticus by Adriane Aytona **

** Activities **

  For this project, I created three activites to try and come up with a messaging app. I created a convoList activity where the user can see all the conversations.There is also the newConvo activity for the user to create a new convversation. The last one is the chat activity where the user is supposed to send messages to a chose conversation.

** convoList **

  For this activity, I had a textview that serves as a title for the activity/screen, button to add a new conversation, a recyclerview that will hold all the conversations, and finally a simple view that acts as a border between the top two elements (title and button) and the recyclerview of conversations.
  In the recyclerview, I put each conversation into a cardview. I was arguing what type of element to use to contain the conversations in this activity; at first I thought of a button, but I thought it will look weird. Then I thought of using a textview that can be clickable but again I thought it will look weird. I was searching for an alternative to both and finally ended with cardview; the end result looks neat and close to what I had in mind.

** newConvo **

  This activity is quite straightforward. The user can type in a title and recipients for a new conversation. I only figured out how to send the title into the POST request but not the recipients. The interesting part I learned while making this activity is the positioning of the Cancel and Confirm buttons. At first, I placed the Confirm button on the left and the Cancel on the right. As I was messing around with the design tab of the xml file, it advised me to put the buttons the other way round. It makes sense since most people are right handed and they use their thumbs most of the time when using their phones; it puts less strain on the thumb to tap something closer to it.

** chat **

  This activity is the saddest of all three. I tried my hardest to implement the GET and POST requests for the messages but I cannot figure it out. Whenever I point to this activity from the other two using a startActivity(), the whole app crashes because of a CredentialsManagerException. I spent a whole day trying to figure what is causing this exception but to no avail. This activity comes up only for less that a second and then crashes down. Hoping that everything would work fine, I went ahead and created some xml layouts and drawables that would have been used in the chat xml file for the invidual messages. If everything would have worked out, I could have easily received the messages and put them to either a sentViewHolder or receivedViewHolder and place them all together into a simple MessageListAdapter to be used in a RecylcerView.

** afterthoughts **

  Any designer out there can easily improve the UI of this application. I just based my decisions on the basic messaging applications I have in my phone. Most of all, if I could have figured out the CredentialsManagerException this application would be a discount messaging app. Unfortunately, it is just a conversation-creating-with-no-recipients-inator.