package edu.lsus.chaticus

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // check if user has invalid credentials?
        if (!app.session.hasValidCredentials()) {
            app.login(this)
        }

        // if valid, go to convoList activity
        else {
            startActivity(convoList.createIntent(this))
        }
    }
}