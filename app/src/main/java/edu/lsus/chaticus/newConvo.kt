package edu.lsus.chaticus

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_new_convo.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class newConvo : AppCompatActivity() {

    companion object {
        // intent for main activity (log in page) to use
        fun createIntent(context: Context) : Intent {
            val intent = Intent(context, newConvo::class.java)
            return intent
        }
    }

    lateinit var convo: Convo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_convo)

        cancelConvoBTN.setOnClickListener {
            startActivity(convoList.createIntent(this))
        }

        confirmConvoBTN.setOnClickListener {
            // checks if title and recipients text boxes are empty
            if(convoRecepients.text.isEmpty() || convoTitle.text.isEmpty()) {
                Toast.makeText(applicationContext, "Must provide title and recipient",
                        Toast.LENGTH_SHORT).show()
            } else {
                // create the new conversation
                convo = Convo(convoTitle.text.toString())

                // POST request to create a new convo
                app.api.newConvo(convo).enqueue(object: Callback<Convo>{
                    override fun onFailure(call: Call<Convo>?, t: Throwable?) {
                        // show a failure message
                        Toast.makeText(applicationContext, "Failed to create new conversation",
                                Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<Convo>?, response: Response<Convo>?) {
                        // finally go to chat activity to start chatting
                        startActivity(chat.createIntent(baseContext,
                                response!!.body()!!.id, convoRecepients.text.toString()))
                    }
                })
            }
        }
    }
}
