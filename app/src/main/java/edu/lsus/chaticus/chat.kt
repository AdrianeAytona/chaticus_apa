package edu.lsus.chaticus

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_chat.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class chat : AppCompatActivity() {

    var id: Int = 0

    companion object {
        // createIntent for conversations in convoList
        fun createIntent(context: Context, convoId: Int) : Intent {
            val intent = Intent(context, chat::class.java)
            intent.putExtra("id", convoId)
            return intent
        }

        // createIntent for new convo activity to use
        fun createIntent(context: Context, convoId: Int, recipients: String?) : Intent {
            val intent = Intent(context, chat::class.java)
            intent.putExtra("recps", recipients)
            intent.putExtra("id", convoId)
            return intent
        }

        fun readRecipients(intent: Intent) : String? {
            return intent.getStringExtra("recps")
        }

        fun readId(intent: Intent) : Int {
            return intent.getIntExtra("id", 0)
        }
    }

    var recipients: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        if (!app.session.hasValidCredentials()) {
            app.login(this)
        }

        /* if valid, get the messages in this conversation -> I STILL DON'T KNOW HOW TO WORK ON THE
                                                                API ENDPOINT like user and etc*/
        else {
            // get the given extras from new convo activity
            id = chat.readId(intent)
            recipientsElem.text = chat.readRecipients(intent)

            // activity comes up for less than a second and then CRASHES!
            app.api.getMessages(id).enqueue(object: Callback<List<MessageDto>> {
                override fun onFailure(call: Call<List<MessageDto>>?, t: Throwable?) {
                    // show failure message
                    Toast.makeText(applicationContext, "Failed to load messages from ConversationID: " + id,
                            Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<List<MessageDto>>?, response: Response<List<MessageDto>>?) {
                    // show success message
                    Toast.makeText(applicationContext, "Can load messages from ConversationID: " + id,
                            Toast.LENGTH_SHORT).show()
                }
            })
        }
    }
}
