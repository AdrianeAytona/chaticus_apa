package edu.lsus.chaticus

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_convo_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.widget.TextView
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.support.v4.content.ContextCompat.startActivity
import android.content.ClipData.Item
import android.support.v4.content.ContextCompat.createDeviceProtectedStorageContext


class convoList : AppCompatActivity() {

    companion object {
        // intent for main activity (log in page) to use
        fun createIntent(context: Context) : Intent {
            val intent = Intent(context, convoList::class.java)
            return intent
        }
    }

    // initialize adapter for recycler view and list
    lateinit var adapter: Adapter
    lateinit var list: List<Convo>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_convo_list)

        // check if user has invalid credentials?
        if (!app.session.hasValidCredentials()) {
            app.login(this)
        }
        // if valid, show list of conversations
        else {
            /* listener for button to start creating a new conversation
             brings up newConvo activity */
            addConvoBTN.setOnClickListener {
                startActivity(newConvo.createIntent(this))
            }

            app.api.getConvos().enqueue(object : Callback<List<Convo>> {
                override fun onFailure(call: Call<List<Convo>>?, t: Throwable?) {
                    Toast.makeText(applicationContext, "Failed to load conversations",
                            Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<List<Convo>>?, response: Response<List<Convo>>?) {
                    // load the conversations
                    list = response!!.body()!!
                    adapter = Adapter(list)
                    convoRecycler.adapter = adapter
                    convoRecycler.layoutManager = LinearLayoutManager(this@convoList)

                    Toast.makeText(applicationContext, "Loaded conversations",
                            Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

    // adapter class for the recycler view
    class Adapter(val items: List<Convo>) : RecyclerView.Adapter<ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
            val inflater = LayoutInflater.from(parent!!.context)
            val view = inflater.inflate(R.layout.convo_cardview, parent, false)
            val vh = ViewHolder(view)
            vh.convoTitleTXT = view.findViewById(R.id.convoTitleTXT)
            vh.convoCreatedTXT = view.findViewById(R.id.convoCreatedTXT)
            vh.convoUpdatedTXT = view.findViewById(R.id.convoUpdatedTXT)
            return vh
        }

        override fun getItemCount(): Int {
            return items.size
        }

        //
        override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
            holder!!.convoId = items[position].id
            holder.convoTitleTXT.text = items[position].title
            holder.convoCreatedTXT.text = items[position].createdAt
            holder.convoUpdatedTXT.text = items[position].updatedAt
        }
    }

    // viewholder class for the recycler view
    class ViewHolder(val root: View) : RecyclerView.ViewHolder(root) {
        var convoId: Int = 0
        lateinit var convoTitleTXT: TextView
        lateinit var convoCreatedTXT: TextView
        lateinit var convoUpdatedTXT: TextView

        // CRASHES
        init {
            root.setOnClickListener {
                root.context.startActivity(chat.createIntent(root.context, convoId))
            }
        }
    }

}
